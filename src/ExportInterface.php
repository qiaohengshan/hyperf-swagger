<?php
namespace joyqhs\Swagger;

interface ExportInterface{
	public function export($apis);
}
