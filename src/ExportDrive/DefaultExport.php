<?php
namespace joyqhs\Swagger\ExportDrive;

class DefaultExport extends \joyqhs\Swagger\AbstractExport
{
    public function export($apis)
    {
        $docs = [
            'project' => config('swagger.api_export_options.joyqhs.project'),
            'docs'  => $apis
        ];
        return $this->save($docs);
    }
}
