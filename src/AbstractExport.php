<?php
namespace joyqhs\Swagger;

abstract class AbstractExport implements ExportInterface
{
    protected function save($docs, $file_name=null)
    {
        if (is_null($file_name)) {
            $file_name = array_search(get_class($this), config('swagger.drives'));
        }
        $file = rtrim(config('swagger.export_path'), '/').'/'.$file_name.'.json';
        return file_put_contents($file, json_encode($docs, 320)) ? $file : false;
    }
}
