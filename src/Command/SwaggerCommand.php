<?php

declare(strict_types=1);

namespace joyqhs\Swagger\Command;

use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputOption;
use joyqhs\Swagger\Swagger;

/**
 * @Command
 */
class SwaggerCommand extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container, Swagger $swagger)
    {
        $this->container = $container;
        $this->swagger = $swagger;
        parent::__construct('swagger:gen');
    }

    public function configure()
    {
        $this->setDescription('Generate documentation through annotations')
            ->addOption('module', 'M', InputOption::VALUE_OPTIONAL, 'Filter routers by use Modules')
            ->addOption('server', 'S', InputOption::VALUE_OPTIONAL, 'Which server you want to describe routes.', 'http')
            ->addOption('path', 'P', InputOption::VALUE_OPTIONAL, 'Get detailed interface information based on the path')
            ->addOption('export', 'E', InputOption::VALUE_OPTIONAL, 'Get detailed interface information based on the path', false);
    }

    public function handle()
    {
        $server = $this->input->getOption('server');
        $path = $this->input->getOption('path');
        $export = $this->input->getOption('export');
        $modules = $this->input->getOption('module');
        $apis = $this->swagger->buildDoc($server, $path,$modules);
        $rows = [];
        foreach ($apis as $api) {
            $params = [];
            foreach ($api['params'] as $param) {
                $params[] = implode(PHP_EOL, [
                    '<error>' . $param['name'] . '</error>',
                    implode(
                        PHP_EOL,
                        collect($param['datas'])->map(function ($item) {
                            return print_r($item, true);
                        })->toArray()
                    )
                ]);
            }
            $rows[] = [
                $api['name'],
                $api['module'],
                $api['method'],
                $api['desc'],
                $api['url'],
                implode(PHP_EOL, $params)
            ];
            $rows[] = new TableSeparator();
        }
        array_pop($rows);

        $table = new Table($this->output);
        $table
            ->setHeaders(['Title', 'Module', 'Method', 'Desc', 'Url', 'Params'])
            ->setRows($rows);
        $table->render();

        $this->output->success('success.');
        if ($export === false) {
            return;
        }
        $this->export($apis, $export);
    }

    protected function export($apis, $drive)
    {
        $drives = config('swagger.drives');
        $list = array_keys($drives);
        if (empty($list)) {
            return;
        }

        if (!array_search($drive, $drives)) {
            $drive = $this->output->choice('export to:', $list, $list[0]);
        }

        $file = (new $drives[$drive])->export($apis);
        if ($file) {
            $this->output->success('export successed. path:' . $file);
        } else {
            $this->output->error('export failed');
        }
    }
}
