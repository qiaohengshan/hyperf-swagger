<?php
namespace joyqhs\Swagger\Annotation;

use Hyperf\Di\Annotation\AbstractAnnotation;

/**
 * @Annotation
 * @Target({"ALL"})
 */
class ApiParams extends AbstractAnnotation
{
    /**
     * 接口参数合集
     * @var String
     */
    public $params = [];

    /**
     * @var String
     * 参数块名称
     */
    public $name="请求参数";
    /**
     * 参数类型
     *   header 1
     *   param 2
     *   response 3
     * @var string
     */
    public $type = "param";

    public function __construct($value = null)
    {
        if (isset($value['value'][0]->params)) {
            $this->type = 'group';
        }
        parent::__construct($value);
        $this->bindMainProperty('params', $value);
    }
}
