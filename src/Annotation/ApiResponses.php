<?php
namespace joyqhs\Swagger\Annotation;

/**
 * @Annotation
 * @Target({"ALL"})
 */
class ApiResponses extends ApiParams
{
    /**
     * @var string
     */
    public $type = "response";
    /**
     * @var String
     * 参数块名称
     */
    public $name="响应参数";
}
