<?php
namespace joyqhs\Swagger\Annotation;

use Hyperf\Di\Annotation\AbstractAnnotation;
/**
 * @Annotation
 * @Target({"METHOD"})
 */
class ApiOperation extends AbstractAnnotation{
	/**
	 * 接口名称
	 * @var String
	 */
	public $name;

    /**
     * 接口描述
     * @var String
     */
    public $desc = '';

    /**
     * 接口类型
     * @var String
     */
    public $type = 'http';

}
