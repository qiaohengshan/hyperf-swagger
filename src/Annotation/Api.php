<?php
namespace joyqhs\Swagger\Annotation;

use Hyperf\Di\Annotation\AbstractAnnotation;
/**
 * @Annotation
 * @Target({"CLASS"})
 */
class Api extends AbstractAnnotation{
	/**
	 * 分组名称
	 * @var String
	 */
	public $name;

	public function __construct( $value=null ){
		$this->bindMainProperty('name',$value);
	}

}
