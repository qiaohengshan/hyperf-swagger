#### 根据注解生成 API 文档
```
# 类的注解  分组
use joyqhs\Swagger\Annotation\Api;
# 方法的注解 接口描述
use joyqhs\Swagger\Annotation\ApiOperation;
# 参数组的注解
use joyqhs\Swagger\Annotation\ApiParams;
# 参数的注解
use joyqhs\Swagger\Annotation\ApiParam;
# 响应注解
use joyqhs\Swagger\Annotation\ApiResponses;
```
#### 参数说明
| 注解 | 参数名称 | 说明 | 其他 |
| -- | --| --| --|
| Api | name | 分组名称| |
| ApiOperation | name | 接口名称| |
| ApiOperation | desc |  接口描述 | |
| ApiParams | name | 参数组名称  | |
| ApiParams | value |  参数组   | 数组,多个ApiParam 或者 ApiParams|
| ApiParam | name | 参数名 | |
| ApiParam | desc | 参数描述 | |
| ApiParam | type | 参数类型 integer string ....|  |
| ApiParam | required | 是否必填 true or false | |
| ApiParam | children | 子参数 ， 一般用于响应结果的多维数组 | 数组 多个 ApiParam |
| ApiResponses | value |  响应组   | 数组,多个ApiParam 或者 ApiParams|

#### 安装
```
	composer require joyqhs/hyperf-swagger
```
#### 配置
```
php bin/hyperf.php vendor:public joyqhs/hyperf-swagger
```
在 config/autoload/swagger.php 中简单配置
#### 使用例子
```
<?php

declare(strict_types=1);

namespace App\Controller;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\GetMapping;

use joyqhs\Swagger\Annotation\Api;
use joyqhs\Swagger\Annotation\ApiOperation;
use joyqhs\Swagger\Annotation\ApiParams;
use joyqhs\Swagger\Annotation\ApiParam;
use joyqhs\Swagger\Annotation\ApiResponses;
/**
 * @Api("用户管理")
 * @Controller
 */
class AuthController extends AbstractController
{
    
     /**
      * @ApiOperation(name="用户登录")
      * @ApiParams({
      *     @ApiParams(
      *         name="请求头",
      *         type="header",{
      *             @ApiParam(name="authorization",type="string",desc="token")
      *    }),
      *    @ApiParams(
      *        name="请求参数",{
      *             @ApiParam(name="username",type="string",desc="用户名"),
      *             @ApiParam(name="password",type="string",desc="密码")
      *    })
      * })
      * @ApiResponses({
      *     @ApiResponses(
      *         name="成功响应",
      *         {
      *             @ApiParam(name="status",type="string",desc="状态码"),
      *             @ApiParam(name="data",type="string",desc="数据合集",children={
      *                 @ApiParam(name="username",type="string",desc="用户名"),
      *                 @ApiParam(name="username",type="string",desc="用户名"),
      *                 @ApiParam(name="username",type="string",desc="用户名"),
      *             })
      *         }
      *     ),
      *     @ApiResponses(
      *         name="失败响应",
      *         {
      *             @ApiParam(name="status",type="string",desc="状态码"),
      *             @ApiParam(name="code",type="string",desc="error"),
      *             @ApiParam(name="message",type="string",desc="错误信息")
      *         }
      *     ),
      * })
      * @GetMapping(path="login")
      */
    public function login($id)
    {

    }
    /**
     * @GetMapping(path="logout")
     * @apiOperation(name="退出",desc="用户退出")
     * @ApiParams(
     *  name="请求头",
     *  type="header",
     *  {
     *  @ApiParam(name="authorization")
     * })
     */
    public function logout(){

    }
    
}

```
#### 查看接口
```
 php bin/hyperf.php swagger:gen 
```
> 可指定参数  -S server.php 中的服务名称 默认 http
-P 路由地址
-E 导出

#### 命令行
```
 php bin/hyperf.php swagger:gen  -S admin
 php bin/hyperf.php swagger:gen  -P auth/login
 php bin/hyperf.php swagger:gen  -S admin -E 
```
#### 展示

![hyperf 注解文档生成](https://images.gitee.com/uploads/images/2020/0702/201459_5fb1571e_1386801.png)

![hyperf 注解文档生成](https://images.gitee.com/uploads/images/2020/0702/201459_cabd20d7_1386801.png)

![hyperf 注解文档生成](https://images.gitee.com/uploads/images/2020/0702/201459_bba55fa8_1386801.png)

![hyperf 注解文档生成](https://images.gitee.com/uploads/images/2020/0702/201459_41821eb4_1386801.png)

![hyperf 注解文档生成](https://images.gitee.com/uploads/images/2020/0702/201459_9480ded2_1386801.png)
#### 注
目前仅支持导出 easydoc.xyz 的 json  api文件
可以在 easydoc.xyz 中导入 实现模拟请求等
后期会支持更多和内置接口展示。
